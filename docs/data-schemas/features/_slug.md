This is the content in the paren-feature-md

```yml
feature-block-hero: 
  feature-name:
  feature-hook: 

feature-block-summary:
  feature-description-header:
  feature-description-body:
  feature-list: # 1-4 items here
    - name: 
      description: 
      icon: 

feature-value-proposition: 
- feature-name:
  feature-icon:
  feature-description:
  feature-list: # feature list should have no more than 4 items
    - feature-list-item: 

feature-production-direction:
  feature-direction-name: 
  feature-direciton-body:

feature-explore-related: # 2-6 features
  - related-feature: 
    related-link:
    related-icon: 
```