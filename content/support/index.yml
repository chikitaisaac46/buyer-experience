---
title: Support
description: Visit the GitLab support page to find product support links and to contact the support team.
side_menu:
  links:
  - text: Contact Support
    href: "#contact-support"
    children:
    - text: Issues with billing, purchasing, subscriptions or licenses
      href: "#issues-with-billing-purchasing-subscription-or-licenses"
    - text: "Self-managed (hosted on your own site/server)"
      href: "#self-managed-hosted-on-your-own-siteserver"
    - text: GitLab SaaS (GitLab.com)
      href: "#gitlab-saas-gitlabcom"
    - text: Legacy Plans
      href: "#legacy-plans"
    - text: Partnership Support
      href: "partnership-support"
    - text: Proving your Support Entitlement
      href: "#proving-your-support-entitlement"
  - text: GitLab Support Service Levels
    href: "#gitlab-support-service-levels"
    children:
    - text: Trials Support
      href: "#trials-support"
    - text: Standard Support (Legacy)
      href: "#standard-support-legacy"
    - text: Priority Support
      href: "#priority-support"
    - text: Service Level Agreement (SLA) details
      href: "#service-level-agreement-sla-details"
    - text: Definitions of GitLab Global Support Hours
      href: "#definitions-of-gitlab-global-support-hours"
    - text: Effect on Support Hours if a preferred region for support is chosen
      href: "#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen"
    - text: Phone and video call support
      href: "#phone-and-video-call-support"
  - text: Further resources
    href: "#further-resources"
  - text: How is Support doing?
    href: "#how-is-support-doing"
components:
- name: call-to-action
  data:
    title: Support
    centered_by_default: true
- name: copy
  data:
    block:
    - hide_horizontal_rule: true
      no_margin_bottom: true
      text: |
        GitLab offers a variety of support options for all customers and users on both paid and free tiers. You should be able to find help using the resources linked below, regardless of how you use GitLab. There are many ways to [contact Support](#contact-support){data-ga-name="contact support" data-ga-location="body"}, but the first step for most people should be to [search our documentation](https://docs.gitlab.com){data-ga-name="documentation" data-ga-location="body"}.

        If you can't find an answer to your question, or you are affected by an outage, then customers who are in a **paid** tier should start by consulting our [statement of support](/support/statement-of-support.html){data-ga-name="statement of support" data-ga-location="body"} while being mindful of what is outside of the scope of support. Please understand that any support that might be offered beyond the scope defined there is done at the discretion of the agent or engineer and is provided as a courtesy.

        If you're using one of GitLab's **free** options, please refer to the appropriate section for free users of either [self-managed GitLab](/support/statement-of-support.html#core-and-community-edition-users){data-ga-name="self-managed GitLab" data-ga-location="body"} or [on GitLab.com](/support/statement-of-support.html#free-plan-users){data-ga-name="free plan users" data-ga-location="body"}.

        Note that free GitLab Ultimate Self-managed and SaaS  granted through trials or as part of our [GitLab for Education](/solutions/education/){:data-ga-name="GitLab for education"}{:data-ga-location="body"}, [GitLab for Open Source](/solutions/open-source/){:data-ga-name="GitLab for open source"}{:data-ga-location="body"}, or [GitLab for Startups](/solutions/startups/){:data-ga-name="GitLab for startups"}{:data-ga-location="body"} programs do not come with technical support. If you are facing issues relating to billing, purchasing, subscriptions, or licensing, please refer to [Issues with billing, purchasing, subscriptions or licenses](#issues-with-billing-purchasing-subscriptions-or-licenses){:data-ga-name="billing"}{:data-ga-location="body"}.

        ## Contact Support {#contact-support}

        ### Issues with billing, purchasing, subscriptions or licenses {#issues-with-billing-purchasing-subscription-or-licenses}

        | Plan      | Support level (First Response Time)              | How to get in touch |
        |-----------|----------------------------|---------------------|
        | All plans and purchases | First reply within 8 hours, 24x5 | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="body"} and select "[Licensing and Renewals Problems](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293){:data-ga-name="licensing and renewal problems" data-ga-location="body"}" |

        ### Self-managed (hosted on your own site/server){#self-managed-hosted-on-your-own-siteserver}

        | Plan | Support Level | How to get in touch |
        |------|----------------|---|
        | Free | None | Open a thread in the  [GitLab Community Forum](https://forum.gitlab.com){data-ga-name="forum" data-ga-location="body"} |
        | Premium and Ultimate | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="feature"} | \
        | | Tiered reply times based on [definitions of support impact](/support/definitions#definitions-of-support-impact){data-ga-name="definitions of support impact" data-ga-location="body"} |  For emergency requests, see the note in the [How to Trigger Emergency Support](#how-to-trigger-emergency-support){data-ga-name="emergency support" data-ga-location="body"} |
        | US Federal | [US Federal Support](/support/us-federal-support){data-ga-name="federal support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://gitlab-federal-support.zendesk.com/) | \
        | | | For emergency requests, see the note in the [US Federal Support description]/support/us-federal-support) |

        ### GitLab SaaS (GitLab.com) {#gitlab-saas-gitlabcom}

        | Plan | Support Level | How to get in touch |
        |------|----------------|---|
        | Free |  None | Open a thread in the [GitLab Community Forum](https://forum.gitlab.com){data-ga-name="forum" data-ga-location="SaaS"} |
        | Premium and Ultimate | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="SaaS"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447){data-ga-name="support portal" data-ga-location="SaaS"} | \
        | | Tiered reply times based on [definitions of support impact](/support/definitions#definitions-of-support-impact){data-ga-name="definitions of support impact" data-ga-location="SaaS"} | |

        ### Legacy Plans {#legacy-plans}

        These plans are no longer offered for new purchases, but still maintain their historical support levels.

        | Plan | Support Level | How to get in touch |
        |------|----------------|---|
        | Bronze (GitLab.com) | [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="body"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="legacy"} | \
        | | Reply within 24 hours 24x5 | |
        | Starter (GitLab Self-managed) |  [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="feature"} | Open a Support Ticket on the [GitLab Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="legacy"} | \
        | | Reply within 24 hours 24x5 | |

        ### Partnership Support {#partnership-support}

        If you're a registered [GitLab Partner](https://about.gitlab.com/handbook/resellers/){data-ga-name="GitLab partner" data-ga-location="body"} you'll want to take a look at the relevant [GitLab Support - Partnerships](/handbook/support/partnerships/){data-ga-name="partnership support" data-ga-location="body"} section for detailed instructions for how to get in touch with Support.

        | Type | Support Level | How to get in touch |
        |------|----------------|---|
        | [Open Partners](/handbook/support/partnerships/open.html){data-ga-name="open partners" data-ga-location="body"} | [Standard Support](#standard-support-legacy){data-ga-name="standard support" data-ga-location="partnership"} or [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="partnership"} depending on customer's license type | Open a Support Ticket using the [GitLab Support Portal - Open Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000818199){data-ga-name="support portal" data-ga-location="partnership"} |
        | [Select Partners](/handbook/support/partnerships/select.html){data-ga-name="select partners" data-ga-location="body"} | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="select partnership"} | Open a Support Ticket using the [GitLab Support Portal - Select Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000837100){data-ga-name="support portal" data-ga-location="select partnership"} |
        | [Alliance Partners](/handbook/support/partnerships/alliance.html){data-ga-name="alliance partners" data-ga-location="body"} | [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="alliance partnership"} | Open a Support Ticket using the [GitLab Support Portal - Alliance Partner Form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001172559){data-ga-name="support portal" data-ga-location="alliance partnership"} |

        ### Proving your Support Entitlement {#proving-your-support-entitlement}
        Depending on how you purchased, upon the creation of your first ticket GitLab Support may **not** be able to automatically detect your support entitlement. If that's the case, you will be asked to provide evidence that you have an active license or subscription. [A variety of types of proof](/support/managing-support-contacts.html#proving-your-support-entitlement){data-ga-name="types to proof - support" data-ga-location="body"} are accepted.

        If you want to make sure contacts in your organization aren't required to submit this proof, please see our dedicated page on [Managing Support Contacts](managing-support-contacts.html){data-ga-name="managing support contacts" data-ga-location="body"} for how to get set up with a named set of contacts.

        ## GitLab Support Service Levels {#gitlab-support-service-levels}

        ### Trials Support {#trials-support}

        Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider [contacting Sales](https://about.gitlab.com/sales/){data-ga-name="sales" data-ga-location="body"} to discuss options.

        ### Standard Support (Legacy) {#standard-support-legacy}

        Standard Support is included in Legacy GitLab self-managed **Starter**, and GitLab.com **Bronze** plans. It includes 'Next business day support' which means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours](#definitions-of-gitlab-global-support-hours)).

        Please submit your support request through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="standard support"}. When you receive your license file, you will also receive an email address to use if you need to reach Support and the web form can't be accessed for any reason.

        ### Priority Support {#priority-support}

        Priority Support is included with all self-managed and SaaS GitLab [Premium and Ultimate](/pricing/){data-ga-name="pricing" data-ga-location="body"} purchases. These plans receive **Tiered Support response times**:

        | [Support Impact](/support/definitions#definitions-of-support-impact) | First Response Time SLA  | Hours | How to Submit |
        |-----------------|-------|------|------------------------------------------------|
        | Emergency (Your GitLab instance is completely unusable) | 30 minutes | 24x7 | [Please trigger an emergency](#how-to-trigger-emergency-support){data-ga-name="emergency support" data-ga-location="priority support"} |
        | Highly Degraded (Important features unavailable or extremely slow; No acceptable workaround) | 4 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="high priority support"}. |
        | Medium Impact   | 8 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="medium priority support"}. |
        | Low Impact      | 24 hrs| 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="low priority support"}. |

        Self-managed Premium and Ultimate may also receive:

        - **Support for Scaled Architecture**: A Support Engineer will work with your technical team around any issues encountered after an implementation of a scaled architecture is completed in cooperation with our Customer Success team.
        - **Upgrade Assistance**: Request a review of your plans to move from one version of GitLab to a newer release version. We'll help you through understanding and becoming confident with the process and ensure there aren't any surprises when you kick off your GitLab version upgrade. Learn [how to schedule an upgrade plan review.](scheduling-upgrade-assistance.html#how-do-i-schedule-upgrade-assistance){data-ga-name="schedule upgrade" data-ga-location="body"}

        ##### How to Trigger Emergency Support {#how-to-trigger-emergency-support}

        To trigger emergency support you **must send a new email** to the emergency contact address provided with your GitLab license. When your license file was sent to your licensee email address, GitLab also sent a set of addresses to reach Support for emergency requests. You can also ask your Technical Account Manager or sales rep for these addresses.

        - **Note:** CCing the address on an existing ticket or forwarding it will **not** page the on-call engineer.

        It is preferable to [include any relevant screenshots/logs in the initial email](/support/general-policies#working-effectively-in-support-tickets){data-ga-name="Effective support tickets" data-ga-location="body"}. However if you already have an open ticket that has since evolved into an emergency, please include the relevant ticket number in the initial email.

         - **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com){data-ga-name="GitLab status" data-ga-location="body"} before contacting Support.

        Once an emergency has been resolved, GitLab Support will close the emergency ticket. If a follow up is required post emergency, GitLab Support will either continue the conversation via a new regular ticket created on the customer's behalf, or via an existing related ticket.

        #### Service Level Agreement (SLA) details {#service-level-agreement-sla-details}

        - GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
        - The SLA times listed are the time frames in which you can expect the first response.
        - GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are *not* to be considered as an expected time-to-resolution.

        #### Definitions of GitLab Global Support Hours {#definitions-of-gitlab-global-support-hours}

        - **24x5** - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time.
        - **24x7** - For [Emergency Support](/support/definitions#definitions-of-support-impact){data-ga-name="emergency support" data-ga-location="support hours"} there is an engineer on-call 24 hours a day, 7 days a week.

        These hours are the SLA times when selecting 'All Regions' for 'Preferred Region for Support' within the [GitLab Global Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="support hours"}.

        #### Effect on Support Hours if a preferred region for support is chosen {#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen}

        When submitting a new ticket, you will select a 'preferred region for support'. This helps us assign Support Engineers from your region and means you'll be more likely to receive replies in your business hours (rather than at night).
        If you choose a preferred region, the Support Hours for the purposes of your ticket SLA are as follows:

        - Asia Pacific (APAC): 09:00 to 21:00 AEST (Brisbane), Monday to Friday
        - Europe, Middle East, Africa (EMEA): 08:00 to 18:00 CET Amsterdam, Monday to Friday
        - Americas (AMER): 05:00 to 17:00 PT (US & Canada), Monday to Friday

        Customers who select 'All regions' as their preferred region will receive SLAs of 24x5 as described above.

        ### Phone and video call support {#phone-and-video-call-support}
        GitLab does not offer support via inbound or on-demand calls.

        GitLab Support Engineers communicate with you about your tickets primarily through updates in the tickets themselves. At times it may be useful and important to conduct a call, video call, or screensharing session with you to improve the progress of a ticket. The support engineer may suggest a call for that reason. You may also request a call if you feel one is needed. Either way, the decision to conduct a call always rests with the support engineer, who will determine:

        * whether a call is necessary; and
        * whether we have sufficient information for a successful call.

        Once the decision has been made to schedule a call, the support engineer will:

        1. Send you a link (through the ticket) to our scheduling platform or, in the case of an emergency, a direct link to start the call.
        1. Update you through the ticket with: (a) an agenda and purpose for the call, (b) a list of any actions that must be taken to prepare for the call, and (c) the maximum time allowed for the call. _Please expect that the call will end as soon as the stated purpose has been achieved or the time limit has been reached, whichever occurs first._

        During a screensharing session Support Engineers will act as a trusted advisor: providing troubleshooting steps and inviting you to run commands to help gather data or help resolve technical issues. At no time will a GitLab Support Engineer ask to take control of your computer or to be granted direct access to your GitLab installation.

        **NOTE:** Calls scheduled by GitLab Support are on the Zoom platform. If you cannot use Zoom, you can request a Cisco Webex link. If neither of these work for you, GitLab Support can join a call on the following platforms: Microsoft Teams, Google Hangouts, Zoom, Cisco Webex. Other video platforms are not supported.

        ***Please Note:***  Attempts to reuse a previously-provided scheduling link to arrange an on-demand call will be considered an abuse of support, and will result in such calls being cancelled.

        ## Further resources {#further-resources}

        Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our [get help page](/get-help/){data-ga-name="get help" data-ga-location="body"}.

        Or check our some of our other support pages:

        - [Customer Satisfaction](/support/customer-satisfaction)
        - [General Support Policies](/support/general-policies)
        - [GitLab.com Specific Support Policies](/support/gitlab-com-policies)
        - [Support Definitions](/support/definitions)
        - [Support Portal](/support/portal)
        - [US Federal Support](/support/us-federal-support)
