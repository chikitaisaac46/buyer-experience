feature-block-hero:
  data:
    feature-name: Continuous Integration and Delivery
    feature-hook: Make software delivery repeatable and on-demand
    feature-placeholder-image:
      - >-
        /nuxt-images/solutions/infinity-icon-cropped.svg
feature-overview:
  data:
    content_summary_bolded: GitLab Continuous Integration and Delivery
    content_summary: >-
      automates all the steps required to build, test and deploy your code to your production environment.
      <br><br>
      <b>Continuous integration</b> automates the builds, provides feedback via code review, and automats code quality and security tests. It creates a release package that is ready to be deployed to your production environment.
      <br><br>
      <b>Continuous delivery</b> automatically provisions infrastructure, manages infrastructure changes, ticketing, and release versioning. It allows, progressive code deployment, verifying and monitoring changes made and providing the ability to roll back when necessary. Together, GitLab Continuous Integration and Delivery help you automate your SDLC, making  it repeatable and on-demand with minimal manual intervention.
    content_image: continuous-integration/stockimage-working-topview.jpeg
    content_heading: Automate entire workflows with GitLab CI/CD
    content_list:
      - Improve developer productivity with in-context testing results.
      - Create secure and compliant releases by testing every change.
      - Implement guardrails to safeguard your deployments.
      - Automate consistent pipelines for both simplicity and scale.
      - Automate both applications and infrastructure.
      - Accelerate your cloud native and hybrid cloud journey.
feature-benefit:
  data:
    feature_heading: Automate everything
    feature_cards:
      - feature_name: In-context testing
        feature_icon: automated-code.svg
        feature_description: >-
          Every change triggers an automated pipeline to make your life easier.
        feature_list:
          - Test everything - code, performance, load, security.
          - Results shared in the merge request before the developer switches tasks.
          - Review, collaborate, iterate and approve in-context.
      - feature_name: Secure and compliant
        feature_icon: shield-check.svg
        feature_description: >-
          From compliance pipelines to integrated security scanning—see it all in one place for better visibility and control.
        feature_list:
          - Application security testing, built-into the developers’ workflow.
          - Vulnerability management for the security pro.
          - Approvals, audit reports, and traceability to simplify policy compliance.
      - feature_name: Implement guardrails to safeguard your deployments.
        feature_icon: approve-dismiss.svg
        feature_description: >-
          Avoid failed or non-performant deployments with strategies to safeguard your deployments.
        feature_list:
          - Compliance pipelines to ensure policies are consistently followed.
          - Granular control over what to deploy via feature flags.
          - Decide who to deploy to via progressive delivery, canary and blue-green deployments.
      - feature_name: Pipelines built for both simplicity and scale
        feature_icon: pipeline.svg
        feature_description: >-
          Flexible pipelines to support you at any stage of your journey.
        feature_list:
          - Built-in templates to help you get started easily.
          - Automatically created pipelines via Auto DevOps.
          - Scale with parent-child pipelines and, merge trains.
      - feature_name: Automate applications and infrastructure
        feature_icon: devsecops-loop.svg
        feature_description: >-
          Same platform to automate application and infrastructure.
        feature_list:
          - Minimize license costs and learning curve.
          - Leverage application DevOps best practices for infrastructure.
      - feature_name: Accelerate your cloud native and hybrid cloud journey
        feature_icon: continuous-integration.svg
        feature_description: >-
          GitLab is infrastructure-agnostic DevOps that is built for multicloud.
        feature_list:
          - Supports deploying to virtual machines, Kubernetes clusters, or FaaS from different cloud vendors.
          - Supports Amazon Web Services, Google Cloud Platform, Microsoft Azure, or your own private cloud.     
features-block-related:
  data:
    - Continuous Integration
    - Epic & Issue Boards
    - Security Dashboards
    - Approval Rules
    - Vulnerability Management
solutions-resource-cards:
  data:
    column_size: 4
    cards:
      - icon: /nuxt-images/home/resources/icon_ebook.svg
        event_type: Ebook
        header: How to convince your leadership to adopt CI/CD
        link_text: Read more
        image: "/nuxt-images/resources/resources_1.jpeg"
        href: https://page.gitlab.com/2021_eBook_leadershipCICD.html
        data_ga_name: How to convince your leadership to adopt CI/CD
        data_ga_location: body
      - icon: /nuxt-images/features/resources/icon_case_study.svg
        event_type: Case Study
        header: >-
          Five AWS customers who depend on GitLab for DevOps workflow 
        link_text: Read more
        href: https://about.gitlab.com/resources/ebook-five-aws-customers/
        image: /nuxt-images/features/resources/resources_case_study.png
        data_ga_name: Five AWS customers who depend on GitLab for DevOps workflow
        data_ga_location: body
      - icon: /nuxt-images/features/resources/icon_webcast.svg
        event_type: Webcast
        header: 7 GitLab CI/CD hacks
        link_text: Watch now
        href: https://about.gitlab.com/webcast/7cicd-hacks/
        image: /nuxt-images/features/resources/resources_webcast.png
        data_ga_name: 7 GitLab CI/CD hacks
        data_ga_location: body
      - icon: /nuxt-images/features/resources/icon_video.svg
        event_type: Video
        header: Getting started with GitLab CI/CD 
        link_text: Watch now
        href: https://www.youtube.com/watch?v=sIegJaLy2ug
        image: /nuxt-images/features/resources/resources_webcast.png
        data_ga_name: Getting started with GitLab CI/CD
        data_ga_location: body
      - icon: /nuxt-images/features/resources/icon_video.svg
        event_type: Video
        header: CI/CD Overview
        link_text: Watch now
        href: https://www.youtube.com/watch?v=l5705U8s_nQ&t=397s
        image: /nuxt-images/resources/fallback/img-fallback-cards-cicd.png
        data_ga_name: CI/CD Overview
        data_ga_location: body
      - icon: /nuxt-images/features/resources/icon_case_study.svg
        event_type: Case Study
        header: Auto-scaling to achieve faster pipelines 
        link_text: Read more
        href: https://about.gitlab.com/customers/EAB/
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        data_ga_name: Auto-scaling to achieve faster pipelines
        data_ga_location: body
template: feature
title: 'Continuous Integration and Delivery'
description: 'Make software delivery repeatable and on-demand'
