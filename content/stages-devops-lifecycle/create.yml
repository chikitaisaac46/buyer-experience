---
  title: Create
  description: Learn how GitLab helps create, view, and manage code and project data through powerful branching tools. View more here!
  components:
    - name: sdl-cta
      data:
        title: Create
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Create, view, and manage code and project data through powerful branching tools.
        text: |
          GitLab helps teams design, develop and securely manage code and project data from a single distributed version control system to enable rapid iteration and delivery of business value. GitLab repositories provide a scalable, single source of truth for collaborating on projects and code which enables teams to be productive without disrupting their workflows.
        image:
          url: /nuxt-images/icons/devops/create-colour.svg
          alt: Create
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Source Code Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Source code management enables coordination, sharing, and collaboration across an entire software development team. GitLab supports teams to rapidly collaborate and iterate on new features and deliver business value.
            link:
              href: /stages-devops-lifecycle/source-code-management/
              text: Learn More
              data_ga_name: source code management learn more
              data_ga_location: body
          - title: Code Review
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Review code, discuss changes, share knowledge, and identify defects in code among distributed teams via asynchronous review and commenting. Automate, track and report code reviews.
            link:
              href: /stages-devops-lifecycle/code-review/
              text: Learn More
              data_ga_name: code review learn more
              data_ga_location: body
          - title: Wiki
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Share documentation and organization information with a built in wiki.
            link:
              href: https://docs.gitlab.com/ee/user/project/wiki/
              text: Learn More
              data_ga_name: wiki learn more
              data_ga_location: body
          - title: Web IDE
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              A full featured Integrated Development Environment (IDE) built into GitLab so you can start contributing on day one with no need to spend days getting all the right packages installed into your local dev environment.
            link:
              href: https://docs.gitlab.com/ee/user/project/web_ide/index.html
              text: Learn More
              data_ga_name: web ide learn more
              data_ga_location: body
          - title: Live Preview
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Preview your simple JavaScript apps and static sites in the Web IDE, viewing your changes in real time, right next to the code.
            link:
              href: https://docs.gitlab.com/ee/user/project/web_ide/index.html#live-preview
              text: Learn More
              data_ga_name: live preview learn more
              data_ga_location: body
          - title: Snippets
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Store and share bits of code and text with other users.
            link:
              href: https://docs.gitlab.com/ee/user/snippets.html
              text: Learn More
              data_ga_name: snippets learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/create/){data-ga-name="create direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Plan
            icon:
              url: /nuxt-images/icons/devops/plan-colour.svg
              alt: Create
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/plan/
              data_ga_name: plan
              data_ga_location: body
          - title: Verify
            icon:
              url: /nuxt-images/icons/devops/verify-colour.svg
              alt: Verify
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
          - title: Package
            icon:
              url: /nuxt-images/icons/devops/package-colour.svg
              alt: Package
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Create a consistent and dependable software supply chain with built-in package management.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/package/
              data_ga_name: package
              data_ga_location: body
