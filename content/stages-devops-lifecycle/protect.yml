---
  title: Protect
  description: Learn how GitLab protects your apps and infrastructure from security intrusions. View more here!
  components:
    - name: sdl-cta
      data:
        title: Protect
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Protect your apps and infrastructure from security intrusions.
        text: |
          GitLab provides cloud native protections, including unified policy management, container scanning, and container network and host security.
        image:
          url: /nuxt-images/icons/devops/defend-colour.svg
          alt: Protect
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Container Scanning
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Check Docker images for known vulnerabilities in the application environment. Analyze image contents against public vulnerability databases using the open source tool, Clair, that is able to scan any kind of Docker (or App) image. Vulnerabilities are shown in-line with every merge request.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/container_scanning/
              text: Learn More
              data_ga_name: container scanning learn more
              data_ga_location: body
          - title: Security Orchestation
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Unified policy and alert security orchestration capabilities across all of GitLab's scanners and security technologies.
            link:
              href: https://docs.gitlab.com/ee/user/application_security/threat_monitoring/#configuring-network-policy-alerts
              text: Learn More
              data_ga_name: security orchestation learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/protect/){data-ga-name="protect direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Release
            icon:
              url: /nuxt-images/icons/devops/release-colour.svg
              alt: Release
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/release/
              data_ga_name: release
              data_ga_location: body
          - title: Secure
            icon:
              url: /nuxt-images/icons/devops/secure-colour.svg
              alt: Secure
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Security capabilities, integrated into your development lifecycle.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/secure/
              data_ga_name: secure
              data_ga_location: body
          - title: Manage
            icon:
              url: /nuxt-images/icons/devops/manage-colour.svg
              alt: Manage
            aos_animation: zoom-in
            aos_duration: 1500
            text: Gain visibility and insight into how your business is performing.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/manage/
              data_ga_name: manage
              data_ga_location: body
