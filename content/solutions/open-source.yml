---
  title: GitLab for Open Source
  description: Open Source organizations can access our top tier features, plus 50,000 CI pipeline minutes, for free. Find out more!
  image_title: /nuxt-images/open-graph/open-source-card.png
  image_alt: GitLab for Open Source
  twitter_image: https://about.gitlab.com/nuxt-images/open-graph/open-source-card.png
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab for Open Source
        subtitle: GitLab believes in a world where everyone can contribute and we like to support those who share our mission
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/open-source/join
          text: Join the GitLab for Open Source Program
          data_ga_name: learn more for open source
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab for open source"
    - name: benefits
      data:
        header: About
        cards_per_row: 2
        text_align: left
        aos_animation: fade-up
        aos_duration: 600
        benefits:
          - icon:
              name: ci-cd
              alt: CI-CD Icon
              variant: marketing
              hex_color: '#9B51DF'
            title: Grow your FOSS project
            description: |
              GitLab helps you onboard newcomers to your project faster and more easily than ever with a single tool for all aspects of your software development lifecycle.
              \
              Not only created for developers, GitLab enables cross-functional team collaboration. It’s a perfect tool for Design, Documentaion, Engagement, and Governance teams alike. GitLab’s user-friendly and inclusive design makes it easier to attract and retain contributors from diverse backgrounds to help your open source project thrive.
          - icon:
              name: lock-alt
              alt: Lock Icon
              variant: marketing
              hex_color: '#FBA326'
            title: World-class CI/CD and Security
            description: |
              Continuous Integration (CI) and Continuous Delivery (CD) help you cut down on manual work and increase automation. That way, you can build and deploy great quality software more quickly.
              \
              Since [GitLab CI](/stages-devops-lifecycle/continuous-integration/){data-ga-name="Gitlab CI" data-ga-location="body"} is fully integrated, it allows you to get set up faster and provides a seamless experience. GitLab for Open Source members get 50,000 CI pipeline minutes per month for free.
              \
              Using GitLab CI with one of our top tiers provides you with best-in-class functionality. However, if your FOSS community is using another source code management platform, we still encourage you to use GitLab CI!
              \
              With [Security baked into our CI pipelines](/solutions/dev-sec-ops/){data-ga-name="CI Pipelines security" data-ga-location="body"}, you get a robust set of application security scans. All of the results are provided for development teams in the Merge Request view, and can be managed in the security dashboard by security teams.
              \
              Still deciding? We encourage you to [learn more about how GitLab CI compares](/devops-tools/){data-ga-name="Devops tools" data-ga-location="body"} to other popular CI providers such as Travis CI and Circle CI and see for yourself.
          - icon:
              name: agile-alt-2
              alt: Agile Icon
              variant: marketing
              hex_color: '#FA7035'
            title: Choose the GitLab distribution that's right for your FOSS community
            description: |
              You can choose to host your open source project on GitLab.com or self-host your own instance. Each of these options has [powerful features](/features/){data-ga-name="features" data-ga-location="body"} to enable you to develop enterprise-grade software at scale.
              \
              In alignment with our [Transparency value](/handbook/values/#transparency){data-ga-name="transparency" data-ga-location="body"}, all of GitLab’s code is source-available and the open source components of GitLab are published under an [MIT open source license](https://opensource.org/licenses/MIT){data-ga-name="MIT open source license" data-ga-location="body"}. The [open source distribution](/install/ce-or-ee/){data-ga-name="open source distribution" data-ga-location="body"} is [available to dowload here](/install/?version=ce){data-ga-name="install" data-ga-location="body"} and contains all the same features as our Free tier.
              \
              [GitLab’s Open Source Partners](/solutions/open-source/partners/){data-ga-name="partners" data-ga-location="body"}, are leveraging GitLab’s top tier in both SaaS and self-managed instances, as well as leveraging the power of GitLab’s open source distribution. They serve as examples of how large open source communities can thrive with GitLab.
              \
              If you have any questions, please reach out to us via opensource@gitlab.com.
          - icon:
              name: source-code
              alt: Source Code Icon
              variant: marketing
              hex_color: '#1EBA9E'
            title: "FOSS Spotlight: GNOME"
            video:
              video_url: https://www.youtube-nocookie.com/embed/v6GTrbfe9xk?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: Open Source Partners
        clickable: true
        column_size: 3
        aos_animation: fade
        aos_duration: 300
        button:
          text: Learn more about our OSS Partners
          href: /solutions/open-source/partners/
          data_ga_name: learn more about our OSS partners
          data_ga_location: body
        logos:
          - href: https://www.skatelescope.org/
            logo_url: /nuxt-images/organizations/logo_ska-partner_color.svg
            logo_alt: Ska Telescope
          - href: https://www.gnome.org/
            logo_url: /nuxt-images/organizations/logo_gnome_color.svg
            logo_alt: Gnome
          - href: https://www.debian.org/
            logo_url: /nuxt-images/organizations/logo_debian_color.svg
            logo_alt: Debian
          - href: https://www.kde.org/
            logo_url: /nuxt-images/organizations/logo_kde_color.svg
            logo_alt: KDE
          - href: https://www.drupal.org/
            logo_url: /nuxt-images/organizations/logo_drupal_color.svg
            logo_alt: Drupal
          - href: https://www.freedesktop.org/
            logo_url: /nuxt-images/organizations/logo_freedesktop_color.svg
            logo_alt: Free Desktop
          - href: https://www.x.org/
            logo_url: /nuxt-images/organizations/logo_xorg_color.svg
            logo_alt: X Org
          - href: https://www.xfce.org/
            logo_url: /nuxt-images/organizations/logo_xfce_color.svg
            logo_alt: XFCE
          - href: https://www.finos.org/
            logo_url: /nuxt-images/organizations/logo_finos_color.svg
            logo_alt: Finos
          - href: https://opencores.org/
            logo_url: /nuxt-images/organizations/logo_opencores_color.svg
            logo_alt: Open Cores
          - href: http://www.videolan.org/
            logo_url: /nuxt-images/organizations/logo_vlc_color.svg
            logo_alt: VLC
          - href: https://inkscape.org/
            logo_url: /nuxt-images/organizations/logo_inkscape_color.svg
            logo_alt: Inkscape
          - href: https://developer.arm.com/tools-and-software/open-source-software
            logo_url: /nuxt-images/organizations/logo_arm_color.svg
            logo_alt: ARM
          - href: https://www.kali.org/
            logo_url: /nuxt-images/organizations/logo_kali_color.svg
            logo_alt: Kali
          - href: https://www.haskell.org/
            logo_url: /nuxt-images/organizations/logo_haskell_color.svg
            logo_alt: Haskell
          - href: https://www.synchrotron-soleil.fr/en
            logo_url: /nuxt-images/organizations/logo_soleil_color.svg
            logo_alt: Soleil
          - href: https://archlinux.org/
            logo_url: /nuxt-images/organizations/logo_archlinux_color.svg
            logo_alt: Arch Linux
          - href: https://www.f-droid.org/
            logo_url: /nuxt-images/organizations/logo_fdroid_color.svg
            logo_alt: F-Droid
          - href: https://getfedora.org/
            logo_url: /nuxt-images/organizations/logo_fedora_color.svg
            logo_alt: Fedora
          - href: https://www.centos.org/
            logo_url: /nuxt-images/organizations/logo_centos_color.svg
            logo_alt: CentOS
    - name: pull-quote
      data:
        quote: Free software enables and empowers us as a society to build upon past knowledge and create greater and more impactful technologies.
        source: Carlos Soriano
        cite: BOARD OF DIRECTORS, GNOME
        shadow: true
