---
  title: Solutions
  description: GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster, more efficiently, while strengthening security and compliance. Learn more!
  components:
    - name: 'solutions-call-to-action'
      data:
        title: Solutions
        subtitle: GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster, more efficiently, while strengthening security and compliance.
        centered_by_default: true
        cols: 6
    - name: solutions-featured-media
      data:
        column_size: 6
        media:
          - title: Version Control and Collaboration
            title_xxl: true
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Integrated workflows for crossfunctional teams
            text: |
              Source code management enables coordination, sharing and collaboration across the entire software development team. Track and merge branches, audit changes and enable concurrent work, to accelerate software delivery.
            link:
              href: /solutions/benefits-of-using-version-control/
              text: Learn More
              data_ga_name: Version Control and Collaboration learn more
              data_ga_location: body
          - title: Remote Work
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Collaborate efficiently from anywhere
            text: |
              GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones.
            link:
              href: /solutions/gitlab-for-remote/
              text: Learn More
              data_ga_name: Remote Work learn more
              data_ga_location: body
          - title: Agile Delivery
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Adopt agile software development practices
            text: |
              Agile software development practices help teams break work into smaller increments and accelerate software development and delivery. GitLab's powerful planning features enable you to streamline agile adoption by keeping it closely linked to software delivery.
            link:
              href: /solutions/agile-delivery/
              text: Learn More
              data_ga_name: Agile Delivery learn more
              data_ga_location: body
          - title: Value Stream Management
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Maximize the value of your software factory
            text: |
              Value Stream Management helps enterprises need visualize, measure, and optimize their development and delivery workflows to deliver the greatest amount of customer value.
            link:
              href: /solutions/value-stream-management/
              text: Learn More
              data_ga_name: Value Stream Management learn more
              data_ga_location: body
          - title: DevSecOps
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Improve Quality and Security with DevSecOps
            text: |
              'We’ll fix it in production' is the typical answer to late found defects or security issues on far too many software projects. Because testing and security are separate silos, often after multiple rounds of development work, it means that each defect or vulnerability can cause extensive rework. If only the issue was found when it was created. GitLab enables better quality and improved security by embedding testing, QA and security scans into every pipeline, giving developers the right feedback at the right time.
            link:
              href: /solutions/dev-sec-ops/
              text: Learn More
              data_ga_name: DevSecOps learn more
              data_ga_location: body
          - title: Software Supply Chain Security
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Ensure your software supply chain is secure and compliant
            text: |
              Combine compliance controls and security scans to protect your internal and external source code. Ensure that builds are done in a secure environment and signed so that others can be confident in their authenticity.
            link:
              href: /solutions/supply-chain/
              text: Learn More
              data_ga_name: Software Supply Chain Security learn more
              data_ga_location: body
          - title: Accelerating Delivery
            aos_animation: fade-up
            aos_duration: 500
            subtitle: IT Delivery at the Speed of Business
            text: |
              Every business is really a software business and accelerating software delivery is critical to business success. DevOps is the answer, however often, DevOps leads to fragmented and disconnected tool chains, which increases friction, increases cost and ultimately makes it much harder to collaborate. GitLab’s comprehensive, single application end to end DevOps approach enables teams to accelerate delivery without compromise.
            link:
              href: /solutions/faster-software-delivery/
              text: Learn More
              data_ga_name: Accelerating Delivery learn more
              data_ga_location: body
          - title: Cloud Transformation
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Cloud native for business
            text: |
              Many organizations are migrating to a cloud native approach to application architecture. GitLab can help you with your evolution to building and shipping cloud native apps.
            link:
              href: /solutions/cloud-native/
              text: Learn More
              data_ga_name: Cloud Transformation learn more
              data_ga_location: body
          - title: Day 2 Operations
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Support for teams at all stages of maturity
            text: |
              Many tools struggle with serving new users who want to get code deployed as well as they serve experienced teams. Those teams need robust processes for building and deploying their applications. GitLab balances that by relying on small primitives that allow for gradual customization.
            link:
              href: /solutions/day-2-ops/
              text: Learn More
              data_ga_name: Day 2 Operations learn more
              data_ga_location: body
          - title: Software-defined Infrastructure
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Streamlined no-ticket provisioning
            text: |
              Too often, projects are delayed waiting for infrastructure and environments to be provisioned. GitLab can help you move to a streamlined process and simplify the value stream to deliver business solutions with no ticket provisioning.
          - title: Application Performance Management
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Integrated APM
            text: |
              Operations is often viewed in a silo and considered as an after-thought when deploying new versions of software. However, due to lack of end to end visibility, this approach frequently leads to increased mean time to repair. GitLab can help you integrate application performance management with your end to end DevOps lifecycle helping you achieve improved performance and availability.  
          - title: Manage Compliance with GitLab
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Compliance and Governance at DevOps Velocity
            text: |
              Worried about your next SDLC audit? Are your teams following the right controls and documenting every change? In many cases compliance is an expensive and error prone activity, where the stakes are incredibly high. GitLab’s workflows, audit trails and clear approval process makes it natural and easy to deliver at the speed of DevOps with compliance built in. See how GitLab can help with PCI Compliance, HIPAA Compliance, Financial Services Regulatory Compliance, GDPR, IEC 62304:2006, ISO 13485:2016, and ISO 26262-6:2018.
            link:
              href: /solutions/compliance/
              text: Learn More
              data_ga_name: Manage Compliance with GitLab learn more
              data_ga_location: body
          - title: Supporting Geographically distributed teams
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Maximize Remote Developer Productivity
            text: |
              Often development teams work in remote offices around the globe, creating unique collaboration challenges and overhead in keeping them and their work in sync with each other. GitLab simplifies the challenges of leading and managing remote teams, making it easier for them to stay current and working on the latest version of their code.
            link:
              href: /solutions/geo/
              text: Learn More
              data_ga_name: Supporting Geographically distributed teams learn more
              data_ga_location: body
          - title: Reference Architectures
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Maximize Remote Developer Productivity
            text: |
              GitLab is flexible. Whether you’re a business just starting out, or a complex enterprise spread over multiple continents with thousands of developers, GitLab can scale to your needs and uptime requirements.
          - title: Team / Development Platform
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Create a productive and efficient workspace for delivery teams.
            text: |
              GitLab is a single application supporting the entire development lifecycle. As such, GitLab helps teams connect, collaborate and accelerate delivery, helping to meet your business goals and improve team satisfaction.
          - title: Amazon Web Services
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab deploys your applications to AWS.
            text: |
              GitLab is a single application supporting the entire development lifecycle. As such, GitLab helps teams connect, collaborate and accelerate delivery, helping to meet your business goals and improve team satisfaction.
            link:
              href: /partners/technology-partners/aws/
              text: Learn More
              data_ga_name: Amazon Web Services learn more
              data_ga_location: body
          - title: Google Cloud Platform
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab deploys your applications to Google Cloud (GCP) with tight Google Kubernetes Engine (GKE) integration.
            text: |
              GitLab is a single application supporting the entire development lifecycle. As such, GitLab helps teams connect, collaborate and accelerate delivery, helping to meet your business goals and improve team satisfaction.
            link:
              href: /partners/technology-partners/google-cloud-platform/
              text: Learn More
              data_ga_name: Google Cloud Platform learn more
              data_ga_location: body
          - title: Education
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab solutions for education.
            text: |
              Educational institutions around the world are bringing the digital transformation to their campus with the only complete DevOps platform. Learn more about how GitLab is leveraged to transform teaching and learning, advance scientific research, and modernize campus infrastructure.
            link:
              href: /solutions/education/campus/
              text: Learn More
              data_ga_name: Education learn more
              data_ga_location: body
          - title: GitLab Flow
            aos_animation: fade-up
            aos_duration: 500
            subtitle: A simple Git flow enabled by GitLab.
            text: |
              The GitLab workflow facilitates improved team collaboration by accelerating ideas to production with features such as Auto DevOps.
            link:
              href: /solutions/gitlab-flow/
              text: Learn More
              data_ga_name: GitLab Flow learn more
              data_ga_location: body
          - title: Public sector
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab for the public sector.
            text: |
              Bring the power of Git and CI/CD to your federal, state, and education IT teams. Deploy GitLab in your FedRAMP cloud service of choice.
            link:
              href: /solutions/public-sector/
              text: Learn More
              data_ga_name: Public sector learn more
              data_ga_location: body
          - title: Open source
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab is committed to open source.
            text: |
              Learn about major open source projects using GitLab as well as how to obtain free licenses of GitLab Ultimate for open source projects as part of our commitment to open source.
            link:
              href: /solutions/open-source/
              text: Learn More
              data_ga_name: Open source learn more
              data_ga_location: body
          - title: InnerSource
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab is tailor-made for InnerSource.
            text: |
              Learn about the power of deploying open source but within your organization, without giving away your source code.
            link:
              href: /solutions/innersource/
              text: Learn More
              data_ga_name: InnerSource learn more
              data_ga_location: body
          - title: Data Science
            aos_animation: fade-up
            aos_duration: 500
            subtitle: GitLab helps teams connect, collaborate, and accelerate.
            text: |
              GitLab helps data science teams align, organize, and accelerate with team collaboration, version control, agile project management, and automation that remove organizational barriers and friction so you can move faster, without sacrificing quality and reproducibility.
            link:
              href: /solutions/data-science/
              text: Learn More
              data_ga_name: Data Science learn more
              data_ga_location: body
          - title: Faster Code Reviews
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Write better code, improve code quality and implement controls.
            text: |
              With GitLab, reviews are baked into every part of the development process - so that teams can collaborate together to improve the overall quality of code and implement controls to ensure requirements are adequately implemented in the code.
            link:
              href: /solutions/code-reviews
              text: Learn More
              data_ga_name: Faster Code Reviews learn more
              data_ga_location: body
          - title: Self-managed reliability
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Self-managed availability, performance and scale
            text: |
              Ensure disaster recovery, high availability and load balancing of your self-managed deployment.
          - title: Security risk mitigation
            aos_animation: fade-up
            aos_duration: 500
            subtitle: Security policies, alerts, and approvals
            text: |
              Manage your organization's security policies, alerts, and approval rules.
          - title: File locking
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Better version control for text and binary files.
            link:
              href: /solutions/file-locking/
              text: Learn More
              data_ga_name: File locking learn more
              data_ga_location: body
          - title: CI/CD for GitHub
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Host your code on GitHub. Verify, Package, Release, Configure, and Monitor on GitLab.
            link:
              href: /solutions/github/
              text: Learn More
              data_ga_name: GitHub learn more
              data_ga_location: body
          - title: Jenkins integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Trigger a Jenkins build for every push to your GitLab Projects.
            link:
              href: /solutions/jenkins/
              text: Learn More
              data_ga_name: Jenkins learn more
              data_ga_location: body
          - title: Jira integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab's JIRA Integration features ensures developers in GitLab and project managers in JIRA remain in tune.
            link:
              href: /solutions/jira/
              text: Learn More
              data_ga_name: Jira learn more
              data_ga_location: body
          - title: Subgroups
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Manage large projects easily with Subgroups.
            link:
              href: /solutions/subgroups/
              text: Learn More
              data_ga_name: Subgroups learn more
              data_ga_location: body
          - title: Time tracking
            aos_animation: fade-up
            aos_duration: 500
            text: |
              GitLab's Time Tracking feature is built into GitLab so your team can easily estimate and record the time spent on issues and merge requests.
            link:
              href: /solutions/time-tracking/
              text: Learn More
              data_ga_name: Time tracking learn more
              data_ga_location: body
          - title: Remove software bottlenecks
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Together, your leadership and GitLab can enable and accelerate a DevOps transformation.
            link:
              href: /solutions/remove-bottlenecks/
              text: Learn More
              data_ga_name: Remove software bottlenecks learn more
              data_ga_location: body
          - title: GitLab on VMware Tanzu
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Paired with the flexibility and portability of VMware Tanzu, GitLab’s suite of features will further enable development and operations teams to work together, and work smarter.
            link:
              href: /partners/technology-partners/vmware-tanzu/
              text: Learn More
              data_ga_name: VMware Tanzu learn more
              data_ga_location: body
 
