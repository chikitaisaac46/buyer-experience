<script lang="ts">
import CopyButton from '~/components/install/copy-button.vue';

export default {
  components: { CopyButton },
  props: {
    data: {
      type: Object,
      required: true,
      default: null,
    },
    id: {
      type: String,
      required: true,
      default: null,
    },
  },
};
</script>

<template>
  <article :id="id" class="install-dropdown">
    <div>
      <button
        aria-label="Close installation instructions"
        class="close"
        type="button"
        @click="$emit('closed')"
      >
        Close
        <SlpIcon name="close" variant="product" />
      </button>
    </div>
    <div v-if="data.tip" class="tip" v-html="$md.render(data.tip)"></div>
    <div>
      <SlpTypography tag="h4" variant="heading4">
        1. Install and configure the necessary dependencies
      </SlpTypography>
      <SlpTypography v-if="data.dependency_text" tag="p">
        {{ data.dependency_text }}
      </SlpTypography>
      <div class="code-block">
        <pre class="command" v-html="$md.render(data.dependency_command)"></pre>
        <CopyButton :target="data.dependency_command" />
      </div>
      <SlpTypography tag="p"
        >Next, install Postfix to send notification emails. If you want to use
        another solution to send emails please skip this step and
        <a
          href="https://docs.gitlab.com/omnibus/settings/smtp.html"
          data-ga-name="smtp"
          :data-ga-location="data.id"
          >configure an external SMTP server after GitLab has been installed.</a
        ></SlpTypography
      >
      <div class="code-block">
        <pre class="command" v-html="$md.render(data.postfix_command)"></pre>
        <CopyButton :target="data.postfix_command" />
      </div>
      <SlpTypography tag="p"
        >During Postfix installation a configuration screen may appear. Select
        'Internet Site' and press enter. Use your server's external DNS for
        'mail name' and press enter. If additional screens appear, continue to
        press enter to accept the defaults.</SlpTypography
      >
      <SlpTypography tag="h4" variant="heading4">
        2. Add the GitLab package repository and install the package
      </SlpTypography>
      <div class="code-block">
        <pre class="command" v-html="$md.render(data.download_command)"></pre>
        <CopyButton :target="data.download_command" />
      </div>
      <SlpTypography
        v-if="data.preinstall_note"
        tag="p"
        v-html="$md.render(data.preinstall_note)"
      ></SlpTypography>
      <div v-if="data.preinstall_command" class="code-block">
        <pre class="command" v-html="$md.render(data.preinstall_command)"></pre>
        <CopyButton :target="data.preinstall_command" />
      </div>
      <SlpTypography tag="p">
        Next, install the GitLab package. Make sure you have correctly
        <a
          href="https://docs.gitlab.com/omnibus/settings/dns.html"
          data-ga-name="dns"
          :data-ga-location="data.id"
          >set up your DNS</a
        >, and change
        <pre>https://gitlab.example.com</pre>
        to the URL at which you want to access your GitLab instance.
        Installation will automatically configure and start GitLab at that URL.
      </SlpTypography>
      <SlpTypography tag="p">
        For
        <pre>https://</pre>
        URLs, GitLab will automatically
        <a
          href="https://docs.gitlab.com/omnibus/settings/ssl.html#lets-encrypthttpsletsencryptorg-integration"
          data-ga-name="lets encrypt"
          :data-ga-location="data.id"
          >request a certificate with Let's Encrypt</a
        >, which requires inbound HTTP access adn a
        <a
          href="https://docs.gitlab.com/omnibus/settings/dns.html"
          data-ga-name="dns"
          :data-ga-location="data.id"
          >valid hostname</a
        >. You can also
        <a
          href="https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https"
          data-ga-name="configure https"
          :data-ga-location="data.id"
          >use your own certificate</a
        >
        or just use
        <pre>http://</pre>
        (without the
        <pre>s</pre>
        ).
      </SlpTypography>
      <SlpTypography tag="p">
        If you would like to specify a custom password for the initial
        administrator user (
        <pre>root</pre>
        ), check the
        <a
          href="https://docs.gitlab.com/omnibus/installation/index.html#set-up-the-initial-password"
          data-ga-name="set up password"
          :data-ga-location="data.id"
          >documentation</a
        >. If a password is not specified, a random password will be
        automatically generated.
      </SlpTypography>
      <div class="code-block">
        <pre class="command" v-html="$md.render(data.install_command)"></pre>

        <CopyButton :target="data.download_command" />
      </div>
      <SlpTypography tag="h4" variant="heading4">
        3. Browse to the hostname and login
      </SlpTypography>
      <SlpTypography tag="p">
        Unless you provided a custom password during installation, a password
        will be randomly generated and stored for 24 hours in
        <pre>/etc/gitlab/initial_root_password</pre>
        . Use this password with username
        <pre>root</pre>
        to login.
      </SlpTypography>
      <SlpTypography tag="p">
        See our
        <a
          href="https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package"
          data-ga-name="omnibus package"
          :data-ga-location="data.id"
          >documentation for detailed instructions on installing and
          configuration</a
        >.
      </SlpTypography>
      <SlpTypography tag="h4" variant="heading4">
        4. Set up your communication preferences
      </SlpTypography>
      <SlpTypography tag="p">
        Visit our
        <a
          href="https://about.gitlab.com/company/preference-center/"
          data-ga-name="email subscription preference center"
          :data-ga-location="data.id"
          >email subscription preference center</a
        >
        to let us know when to communicate with you. We have an explicit email
        opt-in policy so you have complete control over what and how often we
        send you emails.
      </SlpTypography>
      <SlpTypography tag="p">
        Twice a month, we send out the GitLab news you need to know, including
        new features, integrations, docs, and behind the scenes stories from our
        dev teams. For critical security updates related to bugs and system
        performance, sign up for our dedicated security newsletter.
      </SlpTypography>
      <div class="alert">
        <p>
          <strong>Important note:</strong> If you do not opt-in to the security
          newsletter, you will not receive security alerts.
        </p>
      </div>
      <SlpTypography tag="h4" variant="heading4">
        5. Recommended next steps
      </SlpTypography>
      <SlpTypography tag="p">
        After completing your installation, consider the
        <a
          href="https://docs.gitlab.com/ee/install/next_steps.html"
          data-ga-name="next steps"
          :data-ga-location="data.id"
          >recommended next steps, including authentication options and sign-up
          restrictions.</a
        >
      </SlpTypography>
      <div class="extra-links">
        <a
          v-if="data.id == 'raspberry-pi-os'"
          href="https://docs.gitlab.com/omnibus/settings/rpi.html"
          data-ga-name="Raspberry Pi settings"
          data-ga-location="raspberry pi"
          >Raspberry Pi settings</a
        >
        <a
          href="https://docs.gitlab.com/omnibus/"
          data-ga-name="troubleshooting"
          :data-ga-location="data.id"
          >Troubleshooting</a
        >
        <a
          href="http://docs.gitlab.com/omnibus/manual_install.html"
          data-ga-name="manual installation"
          :data-ga-location="data.id"
          >Manual Installation</a
        >
        <!-- We only support CE in Raspberry Pi -->
        <a
          v-if="data.line_back"
          :href="`/install/ce-or-ee/?distro=#${data.line_back}`"
          data-ga-name="CE or EE"
          :data-ga-location="data.id"
        >
          CE or EE
        </a>
      </div>
    </div>
  </article>
</template>

<style scoped lang="scss">
.install-dropdown {
  border: 1px solid $color-surface-400;
  padding: $spacing-32;
  scroll-margin-top: 0;
  background: $color-surface-50;

  .close {
    float: right;
    border: none;
    display: flex;
    align-items: center;
    cursor: pointer;
  }

  h4 {
    margin: $spacing-16 0;
  }

  pre {
    display: inline-block;
  }

  pre.command,
  pre.command ::v-deep pre {
    white-space: pre-line;
    word-break: break-all;
    word-wrap: break-word;
    position: relative;
    margin-top: $spacing-8;

    code {
      padding: 0;
    }
  }

  .code-block {
    display: flex;
    align-items: center;
    justify-content: space-between;
    position: relative;
  }

  .tip {
    border-left: 5px solid $color-info-300;
    padding: $spacing-16;
    margin-bottom: $spacing-32;
  }

  .alert {
    border-left: 5px solid $color-alert-300;
    padding: $spacing-16;
    margin: $spacing-24 0;
  }

  p {
    margin: $spacing-8 0;
  }

  .extra-links {
    margin-top: $spacing-48;
    display: flex;

    a {
      margin-right: $spacing-24;
      font-size: $text-body-3;
    }
  }
}
</style>
